<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Product\Product;
use Bitm\Utility\Utility;
use Bitm\Cart\Cart;

$product = new Product();
$product = $product->show($_POST['id']);


$data = $_POST;
$data['sid'] = $_SESSION['guest_user'] ;
$data['product_id'] = $product['id'] ;
$data['product_title'] = $product['title'] ;
$data['picture'] = $product['picture'] ;
$data['unit_price'] = $product['mrp'] ;
$data['total_price'] = $product['mrp']  * $_POST['qty'];
$data['qty'] = $_POST['qty'] ;

$cart = new Cart();
$result = $cart->store($data);
if($result){
  //create message
  header("location:cart.php");
}else{
  //generate error message
}
