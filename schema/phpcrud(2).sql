-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 23, 2019 at 03:26 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpcrud`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `promotional_message` varchar(255) DEFAULT NULL,
  `html_banner` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_draft` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `is_deleted`, `created_at`, `modified_at`) VALUES
(1, 'First slide label', '1555670187_sports-q-c-640-480-5.jpg', 'google.com', 'Nulla vitae elit libero, a pharetra augue mollis interdum', '\"banner content goes here\"', 1, 0, 0, '2019-04-19 10:12:47', '2019-04-19 10:36:27'),
(2, 'Second slide label', '1555670176_people-q-c-640-480-6.jpg', 'yahoo.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '\"banner content goes here\"', 1, 0, 0, '2019-04-19 10:17:37', '2019-04-19 10:36:16'),
(3, 'Third slide label', '1555669144_sports-q-c-640-480-5.jpg', 'dsfgsgfds', 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.', 'banner content goes here', 0, 0, 0, '2019-04-19 10:19:04', '2019-04-19 10:19:04'),
(4, '4th banner label', '1555669185_people-q-c-640-480-6.jpg', 'asdkfj', '4th banner message', 'banner content goes here', 0, 0, 0, '2019-04-19 10:19:45', '2019-04-19 10:19:45'),
(5, '5th', '1555669483_technics-q-c-200-200-8.jpg', 'adsf', 'asdf', 'banner content goes here', 0, 0, 0, '2019-04-19 10:24:43', '2019-04-19 10:24:43'),
(6, 'asdfadfs', '1555669511_sports-q-c-640-480-5.jpg', 'asdfasdf', 'sadfasdf', 'banner content goes here', 1, 0, 0, '2019-04-19 10:25:11', '2019-04-19 10:25:11');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `sid`, `product_id`, `picture`, `product_title`, `qty`, `unit_price`, `total_price`) VALUES
(4, 'guest_user_1556024051', 33, '1555671339_sports-q-c-200-200-3.jpg', 'active product', 2, 1111, 2222),
(5, 'guest_user_1556024051', 31, '1556009090_sports-q-c-640-480-5.jpg', 'iPhone XR, 3GB RAM, 128GB ROM', 1, 10999, 10999);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `soft_delete` tinyint(1) NOT NULL DEFAULT '0',
  `is_draft` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `link`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(1, 'Men', '0', 0, 0, '2019-04-19 09:07:23', '2019-04-19 09:07:23'),
(2, 'Women', '/women', 0, 0, '2019-04-19 09:07:58', '2019-04-19 09:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE `phonebooks` (
  `id` int(11) NOT NULL,
  `number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cost` float DEFAULT NULL,
  `mrp` float DEFAULT '0',
  `special_price` float DEFAULT NULL,
  `is_new` tinyint(4) DEFAULT NULL,
  `is_draft` tinyint(4) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `total_sales` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand`, `category`, `title`, `picture`, `short_description`, `description`, `cost`, `mrp`, `special_price`, `is_new`, `is_draft`, `is_active`, `total_sales`, `is_deleted`, `created_at`, `modified_at`) VALUES
(25, NULL, NULL, 'new product with price tiger', '1555671400_sports-q-c-640-480-5.jpg', NULL, NULL, NULL, 14.24, NULL, 1, NULL, 1, NULL, 0, NULL, '2019-04-19 10:56:40'),
(26, NULL, NULL, 'tiger2', '1555671408_fashion-q-c-640-480-9.jpg', NULL, NULL, NULL, 100, NULL, 1, NULL, 1, NULL, 0, NULL, '2019-04-19 10:56:48'),
(27, NULL, NULL, 'animal2', '1556009114_sports-q-c-640-480-5.jpg', NULL, NULL, NULL, 1212, NULL, NULL, NULL, 1, NULL, 0, NULL, '2019-04-23 08:45:14'),
(28, NULL, NULL, 'aadfadf', '1556009106_sports-q-c-200-200-3.jpg', NULL, NULL, NULL, 34234, NULL, NULL, NULL, 1, NULL, 0, NULL, '2019-04-23 08:45:06'),
(29, NULL, NULL, 'asdf', '1556009096_technics-q-c-200-200-8.jpg', NULL, NULL, NULL, 343, NULL, NULL, NULL, 1, NULL, 0, NULL, '2019-04-23 08:44:56'),
(31, NULL, NULL, 'iPhone XR, 3GB RAM, 128GB ROM', '1556009090_sports-q-c-640-480-5.jpg', NULL, NULL, NULL, 10999, NULL, NULL, NULL, 1, NULL, 0, NULL, '2019-04-23 09:17:02'),
(32, NULL, NULL, 'latest one', '1555062568_animals-q-c-200-200-3.jpg', NULL, NULL, NULL, 2222, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL),
(33, NULL, NULL, 'active product', '1555671339_sports-q-c-200-200-3.jpg', NULL, NULL, NULL, 1111, NULL, 1, NULL, 1, NULL, 0, NULL, '2019-04-19 10:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `wearelazies`
--

CREATE TABLE `wearelazies` (
  `id` int(11) NOT NULL,
  `lazy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phonebooks`
--
ALTER TABLE `phonebooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wearelazies`
--
ALTER TABLE `wearelazies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `phonebooks`
--
ALTER TABLE `phonebooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `wearelazies`
--
ALTER TABLE `wearelazies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
