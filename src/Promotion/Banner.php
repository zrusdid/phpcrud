<?php
namespace Bitm\Promotion;

use Bitm\Db\Db;
use PDO;
use Bitm\Utility\Message;
class Banner
{
    public $id;
    public $title;
    public $picture;
    public $link;
    public $promotional_message;
    public $html_banner;
    public $is_draft;
    public $is_active;
    public $is_deleted;
    public $created_at;
    public $modified_at;
    public $conn = null;

    function __construct()
    {
        $this->conn = Db::connect();
    }

    function all($number=3){
        $query = "SELECT * FROM banners WHERE is_deleted = 0 AND is_active = 1 ORDER BY id DESC LIMIT 0,$number ";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    function store($data){

        $this->prepare($data);
        $query = "INSERT INTO `banners` ( 
                    `title`, 
                    `picture`, 
                    `link`, 
                    `is_active`,
                    `promotional_message`, 
                    `html_banner`,
                    `created_at`, 
                    `modified_at`) 
              VALUES ( 
                  :title, 
                  :picture, 
                  :link, 
                  :is_active,
                  :promotional_message, 
                  :html_banner,
                  :created_at,
                  :modified_at)";


        $sth = $this->conn->prepare($query);
        $sth->bindParam(':title',$this->title);
        $sth->bindParam(':picture',$this->picture);
        $sth->bindParam(':link',$this->link);
        $sth->bindParam(':is_active',$this->is_active);
        $sth->bindParam(':promotional_message',$this->promotional_message);
        $sth->bindParam(':html_banner',$this->html_banner);
        $sth->bindParam(':created_at',$this->created_at);
        $sth->bindParam(':modified_at',$this->modified_at);
        $result = $sth->execute();
        return $result;
    }
    function show($id = null){
        if(empty($id)){
            return;
        }
        $query = 'SELECT * FROM banners WHERE id = :id';
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        $sth->execute();
        $banner = $sth->fetch(PDO::FETCH_ASSOC);

        if(!$banner){
            Message::set('Banner Not Found');
            header('location:index.php');
        }
        return $banner;
    }
    function update($data){
        if(empty($data['id'])){
            return;
        }
        $this->prepare($data);

        $query = "UPDATE `banners` SET 
                    `title` = :title, 
                    `link` = :link, 
                    `picture` = :picture, 
                    `is_active` = :is_active, 
                    `promotional_message` = :promotional_message,
                    `html_banner` = :html_banner,
                    `modified_at` = :modified_at
                WHERE `banners`.`id` = :id";


        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$this->id);
        $sth->bindParam(':title',$this->title);
        $sth->bindParam(':link',$this->link);
        $sth->bindParam(':picture',$this->picture);
        $sth->bindParam(':is_active',$this->is_active);
        $sth->bindParam(':promotional_message',$this->promotional_message);
        $sth->bindParam(':html_banner',$this->html_banner);
        $sth->bindParam(':modified_at',$this->modified_at);
        return $sth->execute();
    }

    function trash(){
        $query = "SELECT * FROM banners WHERE is_deleted = 1 ORDER BY id DESC ";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    function softdelete($id){
        if(empty($id)){
            return;
        }

        $query = "UPDATE banners SET is_deleted = 1 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }
    function restore($id){
        if(empty($id)){
            return;
        }
        $query = "UPDATE banners SET is_deleted = 0 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }
    function delete($id){
        if(empty($id)){
            return;
        }

        $query = "DELETE FROM `banners` WHERE `banners`.`id` = :id;";

        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }


    function __toString()
    {
        return $this->title;
    }

    private function prepare($data){

        $this->title = $data['title'];
        $this->picture = $data['picture'];
        $this->link = $data['link'];
        $this->promotional_message = $data['promotional_message'];
        $this->html_banner = $data['html_banner'];
        $this->is_draft = (int) $data['is_draft'];
        $this->is_active = (int) $data['is_active'];
        $this->modified_at = date('Y-m-d h:i:s',time());

        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!$this->id){
            $this->created_at = date('Y-m-d h:i:s',time());
        }
    }
}