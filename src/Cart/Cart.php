<?php
namespace Bitm\Cart;

use Bitm\Db\Db;
use PDO;
use Bitm\Utility\Message;

class Cart{
    public $id;
    public $sid;
    public $product_id;
    public $product_title;
    public $picture;
    public $unit_price;
    public $total_price;
    public $qty;


    function __construct($id=null)
    {
        $this->conn = Db::connect();
    }

    function all($sid){
        $query = "SELECT * FROM carts WHERE `sid` = :sid ";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':sid',$sid);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    function store($data){
        if($this->is_already_in_cart($data['sid'],$data['product_id'])){
            //update the product qty
            $result = $this->updateQty($data['qty'],$data['product_id'],$data['sid']);
            return $result;
        }else{
        $this->build($data);
        $query="INSERT INTO `carts` (
                        `sid`,
                        `product_id`, 
                        `picture`, 
                        `product_title`,
                        `qty`, 
                        `unit_price`,
                        `total_price`) VALUES ( 
                        :sid , 
                        :product_id ,
                        :picture, 
                        :product_title, 
                        :qty , 
                        :unit_price , 
                        :total_price);";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':sid',$this->sid);
        $sth->bindparam(':product_id',$this->product_id);
        $sth->bindparam(':picture',$this->picture);
        $sth->bindparam(':product_title',$this->product_title);
        $sth->bindparam(':qty',$this->qty);
        $sth->bindparam(':unit_price',$this->unit_price);
        $sth->bindparam(':total_price',$this->total_price);
        $result=$sth->execute();
        return $result;
        }

    }

    public function updateQty($qty,$product_id,$sid){
        $query = "SELECT * FROM carts WHERE `sid` = :sid AND `product_id` = :product_id";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':sid',$sid);
        $sth->bindparam(':product_id',$product_id);
        $sth->execute();
        $cart = $sth->fetch(PDO::FETCH_ASSOC);

        //$final_qty = $qty + $cart['qty'];
        $final_qty =  $qty;
        $total_price = $final_qty * $cart['unit_price'];


        $query = "UPDATE `carts` SET 
                    `qty` = :qty,
                    `total_price` = :total_price
                    
                    WHERE `carts`.`product_id` = :product_id 
                    AND `carts`.`sid` = :sid ";


        $sth = $this->conn->prepare($query);
        $sth->bindParam(':sid',$sid);
        $sth->bindParam(':product_id',$product_id);
        $sth->bindParam(':qty',$final_qty);
        $sth->bindParam(':total_price',$total_price);


        return $sth->execute();
    }

    private function is_already_in_cart($sid,$product_id){
        $query = "SELECT * FROM carts WHERE `sid` = :sid AND `product_id` = :product_id";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':sid',$sid);
        $sth->bindparam(':product_id',$product_id);
        $sth->execute();
        $product = $sth->fetch(PDO::FETCH_ASSOC);
        //return (bool) $product;
        if($product){
            return true;
        }else{
            return false;
        }
    }

    private function build($data){

        $this->product_id = $data['product_id'];
        $this->sid = $data['sid'];
        $this->picture = $data['picture'];
        $this->product_title = $data['product_title'];
        $this->qty = $data['qty'];
        $this->unit_price = $data['unit_price'];
        $this->total_price = $data['total_price'];

        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!$this->id){
            $this->created_at = date('Y-m-d h:i:s',time());
        }
    }

    function delete($product_id){
        $query = "DELETE FROM `carts` WHERE 
`carts`.`product_id` = :product_id
AND `carts`.`sid` = :sid;";

        $sth = $this->conn->prepare($query);
        $sth->bindParam(':product_id',$product_id);
        $sth->bindParam(':sid',$_SESSION['guest_user']);
        return $sth->execute();
    }


}