<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");

use Bitm\Promotion\Banner;
use Bitm\Utility\Message;
$data = $_POST;

$prev_picture_name = $_POST['prev_picture_name'];

$is_active = 0;
if(array_key_exists('is_active',$_POST)){
    $is_active = $_POST['is_active'];
}
$is_uploaded = false;
if($_FILES['picture']['size'] > 0){
$target_file = $_FILES['picture']['tmp_name'];
$filename = time()."_".str_replace(' ','-',$_FILES['picture']['name']);
$dest_file = $_SERVER['DOCUMENT_ROOT'].'/phpcrud/uploads/'.$filename;
$is_uploaded = move_uploaded_file($target_file, $dest_file);
}
if($is_uploaded){
    $data['picture'] = $filename;
}else{
    $data['picture'] = $prev_picture_name;
}

$banner = new Banner();
$result = $banner->update($data);

if($result){
    Message::set('Banner has been updated successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry.. There is a problem. Please try again later');
    //log
    header("location:edit.php");// we will need to pass id
}
