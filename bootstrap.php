<?php
include_once("vendor/autoload.php");
session_start();
if(array_key_exists('guest_user',$_SESSION) && !empty($_SESSION['guest_user'])){
    //echo $_SESSION['guest_user'];
}else{
    $_SESSION['guest_user'] = 'guest_user_'.time();
}

if(array_key_exists('loggedin_user',$_SESSION) && !empty($_SESSION['loggedin_user'])){
    //echo $_SESSION['guest_user'];
}else{
    $_SESSION['loggedin_user'] = false;
}
if(!$_SESSION['loggedin_user']){
    header('location:login.php');
}

define('WEBROOT','http://localhost/phpcrud/');
define('ADMIN','http://localhost/phpcrud/admin/');
define('VIEW','http://localhost/phpcrud/admin/views/');
define('LIB','http://localhost/phpcrud/lib/');
define('CSS','http://localhost/phpcrud/lib/css/');
define('JS','http://localhost/phpcrud/lib/js/');
define('IMG','http://localhost/phpcrud/lib/img/');
define('UPLOADS','http://localhost/phpcrud/uploads/');

define('DOCROOT',$_SERVER['DOCUMENT_ROOT'].'/phpcrud/');
define('ELEMENT',DOCROOT.'admin/views/elements/');

//importing layout
ob_start();
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/admin/views/layouts/admin.php");
$layout = ob_get_contents();
ob_end_clean();
