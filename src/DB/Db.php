<?php

namespace Bitm\Db;
use PDO;
class Db
{
    //connect to database
    public static $servername = "localhost";
    public static $username = "root";
    public static $password = "1234";
    public static $dbname = "phpcrud";


    static function connect(){

        $conn = new PDO("mysql:host=".self::$servername.";dbname=".self::$dbname, self::$username, self::$password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }
}