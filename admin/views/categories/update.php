<?php
$id=$_POST['id'];
$name=$_POST['name'];

$link=$_POST['link'];
$soft_delete=$_POST['soft_delete'];
$is_draft=$_POST['is_draft'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="UPDATE `categories` SET 
`link` = :link, 
`soft_delete` = :soft_delete,
 `is_draft` =:is_draft , 
 `created_at` =:created_at , 
 `modified_at` = :modified_at
  WHERE `categories`.`id` = :id;
";

$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$sth->bindparam(':name',$name);
$sth->bindparam(':link',$link);
$sth->bindparam(':soft_delete',$soft_delete);
$sth->bindparam(':is_draft',$is_draft);
$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);
$result=$sth->execute();

header("location:index.php");