<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Product\Category;
use Bitm\Utility\Message;
$data = $_POST;
$category = new Category();
$result = $category->store($data);

if($result){
    Message::set('Product has been added successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry.. There is a problem. Please try again later');
    //log
    header("location:create.php");
}
