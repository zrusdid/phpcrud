<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Product\Product;
use Bitm\Utility\Message;

if(strtoupper($_SERVER['REQUEST_METHOD']) == 'GET'){
    Message::set("Good Try");
    header('location:index.php');
    exit();
}

$product = new Product();
$result = $product->softdelete($_POST['id']);

if($result){
    Message::set("Product $product->title  has been trashed successfully.");
}else{
    Message::set("Product $product->title has not been trashed successfully.");
}
header("location:index.php");

