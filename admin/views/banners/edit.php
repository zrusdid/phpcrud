<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Promotion\Banner;

$banner = new Banner();
$banner = $banner->show($_GET['id']);

?>

<?php
ob_start();
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

            <form id="banner-entry-form" method="post"
                  action="update.php"
                  enctype="multipart/form-data"
                  role="form">
                 <div class="messages"></div>
<h1>Edit Banner</h1>
                <div class="controls">
                    <div class="row">
                        <input id="id"  value="<?php echo $banner['id']?>" type="hidden" name="id" class="form-control">
                        <input id="id"  value="<?php echo $banner['picture']?>" type="hidden" name="prev_picture_name" class="form-control">

                    </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="title">Enter Banner Title</label>
                                <input id="title"
                                       value="<?php echo $banner['title']?>"
                                       type="text"
                                       name="title"
                                       placeholder="e.g. Bashundhara Tissue"
                                       autofocus="autofocus"
                                       class="form-control">
                                <div class="help-block text-muted">Enter Banner Title</div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"  value="" type="file" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                            <?php
                            if(!empty($banner['picture'])){
                            ?>
                                <img class="img-fluid"
                                     src="<?=UPLOADS;?><?php echo $banner['picture']?>" alt="<?php echo $banner['title']?>">
                            <?php
                            }else{
                             ?>
                              <div>No Image is available. Please upload one</div>
                            <?php
                            }
                            ?>

                        </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="link">Banner Link</label>
                            <input id="link"  value="<?php echo $banner['link']?>" type="text" name="link" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="promotional_message">Promotional Message</label>
                            <input id="promotional_message"  value="<?php echo $banner['promotional_message']?>" type="text" name="promotional_message" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="html_banner">HTMLized Banner</label>
                            <br />
                            <textarea cols="130" id="html_banner" name="html_banner">"<?php echo $banner['html_banner']?>"</textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <?php
                        $checked = '';
                        if($banner['is_active']){
                            $checked = 'checked="checked"';
                        }
                        ?>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_active">Active</label>
                                <input id="is_active" <?=$checked?> value="1" type="checkbox" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <!--                            <div class="form-group form-check">-->
                        <!--                                <input name="is_draft" type="checkbox" class="form-check-input" id="is_draft" value="0">-->
                        <!--                                <label class="form-check-label" for="exampleCheck1">Draft?</label>-->
                        <!--                            </div>-->
                    </div>




                    <button type="submit" class="btn btn-success">
                        Send & Save Banner
                    </button>


                </div>

            </form>
        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
