<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Promotion\Banner;

$banner = new Banner();
$banner = $banner->show($_GET['id']);
?>

<?php
ob_start();
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                <h1 >Banner</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="<?=VIEW;?>banners/index.php" style="color: black">Go to list</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ">

                            <div class="row">
                                <div class="col-sm col-md-6 col-lg-3 ">
                                    <div class="banner">
                                        <a href="#" class="img-prod"><img class="img-fluid" src="<?=UPLOADS;?><?php echo $banner['picture']?>" alt="<?php echo $banner['title']?>">
                                            <span class="status">30%</span>
                                        </a>
                                        <div class="text ">
                                            <h3><a href="#"><?php echo $banner['title']?></a></h3>
                                            <div class="d-flex">

                                                <?php echo $banner['promotional_message']?>
                                                <?php echo $banner['link']?>
                                                <?php
                                                if($banner['is_active']){
                                                   echo "The banner is active";
                                                }else{
                                                    echo "The banner is not active";
                                                }

?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                </div>
            </div>



        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>