<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Product\Product;
use Bitm\Utility\Message;
$data = $_POST;

$target_file = $_FILES['picture']['tmp_name'];
$filename = time()."_".str_replace(' ','-',$_FILES['picture']['name']);
$dest_file = $_SERVER['DOCUMENT_ROOT'].'/phpcrud/uploads/'.$filename;
$is_uploaded = move_uploaded_file($target_file, $dest_file);

if($is_uploaded){
    $data['picture'] = $filename;
}else{
    $data['picture'] = "";
}

$product = new Product();
$result = $product->store($data);

if($result){
    Message::set('Product has been added successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry.. There is a problem. Please try again later');
    //log
    header("location:create.php");
}

