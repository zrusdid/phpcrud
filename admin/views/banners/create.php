<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Utility\Message;
ob_start();
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <?php
            if($message = Message::get()){
                ?>
                <div class="alert alert-success">
                    <?php echo $message;?>
                </div>
                <?php
            }
            ?>
            <form id="banner-entry-form"
                  method="post"
                  action="store.php"
                  enctype="multipart/form-data"
                  role="form">
                <div class="messages"></div>
                <h1>ADD NEW</h1>
                <div class="controls">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="title">Enter Banner Title</label>
                                <input id="title"
                                       value=""
                                       type="text"
                                       name="title"
                                       placeholder="e.g. Bashundhara Tissue"
                                       autofocus="autofocus"
                                       class="form-control">
                                <div class="help-block text-muted">Enter Banner Title</div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"  value="" type="file" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="link">Banner Link</label>
                                <input id="link"  value="" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="promotional_message">Promotional Message</label>
                                <input id="promotional_message"  value="" type="text" name="promotional_message" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="html_banner">HTMLized Banner</label>
                                <br />
                                <textarea cols="130" id="html_banner" name="html_banner">banner content goes here</textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group form-check">
                                <input name="is_active" type="checkbox" checked="checked" class="form-check-input" id="is_active" value="1">
                                <label class="form-check-label" for="is_active">Active?</label>
                            </div>

<!--                            <div class="form-group form-check">-->
<!--                                <input name="is_draft" type="checkbox" class="form-check-input" id="is_draft" value="0">-->
<!--                                <label class="form-check-label" for="exampleCheck1">Draft?</label>-->
<!--                            </div>-->
                        </div>

                    <button type="submit" class="btn btn-success">
                        Send & Save Banner
                    </button>


                </div>

            </form>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>