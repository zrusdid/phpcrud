<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
use Bitm\Promotion\Banner;
use Bitm\Utility\Utility;
use Bitm\Utility\Message;

$banner = new Banner();
$banners = $banner->all();

?>

<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

    <?php
    if($message = Message::get()){
        ?>
        <div class="alert alert-success">
            <?php echo $message;?>
        </div>
        <?php
    }
    ?>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                 <h1 >Banner</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="<?=VIEW;?>banners/active.php" style="color: black">Active Banners</a>
                        | <a href="<?=VIEW;?>banners/inactive.php" style="color: black">In active Banners</a>
                        | <a href="<?=VIEW;?>banners/trash.php" style="color: black">Trash</a>
                      | <a href="<?=VIEW;?>banners/create.php" style="color: black">Add New</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Picture</th>
                                <th>Title</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
<?php
                            if($banners){
                                foreach($banners as $banner){
                                    ?>
                                    <tr class="text-center">
                                        <td class="banner-sl">&nbsp;</td>

                                        <td class="image-prod"><div class="img">
                                                <img src="<?=UPLOADS?><?php echo $banner['picture']?>" 
                                                     width="140px" height="120px">
                                            </div></td>

                                        <td class="banner-name">
                                            <h3><a href="show.php?id=<?php echo $banner['id'] ?>"><?php echo $banner['title'];?></a></h3>

                                        </td>

                                        <td> <a href="<?=VIEW?>banners/edit.php?id=<?php echo $banner['id']?>">Edit</a>
                                            |
                                            <form action="<?=VIEW?>banners/softdelete.php" method="post">
                                                <input type="hidden" name="id" value="<?php echo $banner['id'];?>">
                                                <button type="submit" onclick="return confirm('Are you sure you want to delete?')">Soft Delete</button>
                                        </td>
                                            </form>
                                    </tr>
                                <?php }}else{
                                ?>
                                <tr class="text-center">
                                    <td colspan="5">
                                        There is no banner available. <a href="create.php">Click Here</a> to add a banner.
                                    </td>
                                </tr>
                                <?php
                            }
?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>


