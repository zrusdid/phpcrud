<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");

use Bitm\Product\Product;
use Bitm\Utility\Message;

$id = $_GET['id'];
$product = new Product();
$result = $product->restore($id);

if($result){
    Message::set('Product has been restored successfully.');
    header("location:index.php");
}else{
    Message::set('Sorry.. There is a problem. Please try again later');
    //log
    header("location:trash.php");// we will need to pass id
}
