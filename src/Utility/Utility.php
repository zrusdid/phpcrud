<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 4/19/2019
 * Time: 9:46 AM
 */

namespace Bitm\Utility;


class Utility
{
    public static function d($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    public static function dd($var){
        self::d($var);
        die();
    }
}