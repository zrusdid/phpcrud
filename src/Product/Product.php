<?php
namespace Bitm\Product;

use Bitm\Db\Db;
use PDO;
use Bitm\Utility\Message;
class Product
{
    public $id = null;
    public $brand = null;
    public $category = null;
    public $title = 'N/A';
    public $picture = null;
    public $short_description = null;
    public $description = null;
    public $cost = 0.00;
    public $mrp = 0.00;
    public $special_price = 0.00;
    public $is_new = 0;
    public $is_draft = 0;
    public $is_active = 0;
    public $total_sales = 0;
    public $is_deleted = false;
    public $created_at = null;
    public $modified_at = null;
    public $conn = null;

    function __construct()
    {
        $this->conn = Db::connect();
    }
    
    function all(){
        $query = "SELECT * FROM products WHERE is_deleted = 0 ORDER BY id DESC ";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    function getNewProducts($number=3){
        $query = "SELECT * FROM products WHERE is_new = 1 ORDER BY id DESC LIMIT 0,$number ";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    function trash(){
        $query = "SELECT * FROM products WHERE is_deleted = 1 ORDER BY id DESC ";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    function store($data){

        $this->prepare($data);
        $query = "INSERT INTO `products` ( 
            `brand`, 
            `category`,
             `title`, 
             `picture`, 
             `short_description`, 
             `description`, 
             `cost`, 
             `mrp`, 
             `special_price`, 
             `is_new`, 
             `is_draft`, 
             `is_active`, 
             `total_sales`,
              `is_deleted`, 
              `created_at`, 
              `modified_at`) 
              VALUES (
              NULL, 
              NULL, 
              :title, 
              :picture, 
              NULL, 
              NULL, 
              NULL, 
              :mrp, 
              NULL, 
              NULL,
              NULL, 
              :is_active, 
              NULL, 
              NULL, 
              :created_at, 
              :modified_at);";

        $sth = $this->conn->prepare($query);
        $sth->bindParam(':title',$this->title);
        $sth->bindParam(':mrp',$this->mrp);
        $sth->bindParam(':picture',$this->picture);
        $sth->bindParam(':is_active',$this->is_active);
        $sth->bindParam(':created_at',$this->created_at);
        $sth->bindParam(':modified_at',$this->modified_at);
        $result = $sth->execute();
        return $result;
    }

    function __toString()
    {
       return $this->title;
    }

    function show($id = null){
        if(empty($id)){
            return;
        }
        $query = 'SELECT * FROM products WHERE id = :id';
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        $sth->execute();
        $product = $sth->fetch(PDO::FETCH_ASSOC);

        if(!$product){
            Message::set('Product Not Found');
            header('location:index.php');
        }
        return $product;
    }

    function delete($id){
        if(empty($id)){
            return;
        }

        $query = "DELETE FROM `products` WHERE `products`.`id` = :id;";

        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }

    function softdelete($id){
        if(empty($id)){
            return;
        }

        $query = "UPDATE products SET is_deleted = 1 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }

    function restore($id){
        if(empty($id)){
            return;
        }
        $query = "UPDATE products SET is_deleted = 0 WHERE id = :id";
        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$id);
        return $sth->execute();
    }

    function update($data){
        if(empty($data['id'])){
            return;
        }
        $this->prepare($data);

        $query = "UPDATE `products` SET 
                    `title` = :title ,
                    `picture` = :picture ,
                    `mrp` = :mrp ,
                    `is_active` = :is_active, 
                    `modified_at` = :modified_at 
                    
                    WHERE `products`.`id` = :id";


        $sth = $this->conn->prepare($query);
        $sth->bindParam(':id',$this->id);
        $sth->bindParam(':title',$this->title);
        $sth->bindParam(':picture',$this->picture);
        $sth->bindParam(':mrp',$this->mrp);
        $sth->bindParam(':is_active',$this->is_active);
        $sth->bindParam(':modified_at',$this->modified_at);
        return $sth->execute();
    }



    private function prepare($data){

        $this->title = $data['title'];
        $this->mrp = empty($data['mrp'])?0.00:$data['mrp'];
        $this->picture = $data['picture'];
        $this->is_active = $data['is_active'];
        $this->modified_at = date('Y-m-d h:i:s',time());

        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!$this->id){
            $this->created_at = date('Y-m-d h:i:s',time());
        }
    }
}