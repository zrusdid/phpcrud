<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 4/19/2019
 * Time: 2:55 PM
 */

namespace Bitm\Product;

use Bitm\Db\Db;
use PDO;
use Bitm\Utility\Message;

class Category
{
    public $id;
    public $name;
    public $link;

    function __construct()
    {
        $this->conn = Db::connect();
    }

    public function all(){
        $query="select * from categories";
        $sth = $this->conn->prepare($query);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    function store($data){

        $this->prepare($data);
        $query="INSERT INTO `categories` ( 
                                    `name`, 
                                    `link`, 
                                     `created_at`,
                                      `modified_at`) 
                        VALUES (:name, 
                        :link, 
                       
                        :created_at, 
                        :modified_at)";
        $sth = $this->conn->prepare($query);
        $sth->bindparam(':name',$this->name);
        $sth->bindparam(':link',$this->link);
        $sth->bindparam(':created_at',$this->created_at);
        $sth->bindparam(':modified_at',$this->modified_at);
        $result=$sth->execute();
        return $result;
    }

    private function prepare($data){

        $this->name = $data['name'];
        $this->link = empty($data['link'])?0.00:$data['link'];
        $this->modified_at = date('Y-m-d h:i:s',time());

        if(array_key_exists('id',$data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!$this->id){
            $this->created_at = date('Y-m-d h:i:s',time());
        }
    }
}