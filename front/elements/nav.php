<?php
use Bitm\Product\Category;
use Bitm\Utility\Utility;

$category = new Category();
$categories = $category->all();
?>

<ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="products.php">All Products</a>
    </li>
   <?php
   foreach($categories as $category):
   ?>
    <li class="nav-item">
        <a class="nav-link" href="#"><?=$category['name']?></a>
    </li>
    <?php
    endforeach;
    ?>
</ul>